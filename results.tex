\vspace*{-0.4em}
\section{Results}
\label{sec:results}

This section evaluates \wrgc collectors using emulation.  We first compare
emulation results to results of simulation and architecture-independent
analysis. Emulation results match prior results boosting our confidence in our
newly proposed methodology.  We use emulation to explore a richer space of
software configurations and workloads.  This enables us to discuss previously
unseen writes to memory due to interference patterns in multiprogrammed
workloads, simpler heap organizations for graph applications, and the
implications of production datasets. We conclude this section with reporting
raw write rates and PCM lifetimes in years.

\vspace*{-0.4em}
\subsection{Quantitative Comparison of Evaluation Methodologies}

Each evaluation methodology for hybrid memories has its strengths and weaknesses. 
Simulation models real hardware features but limits evaluation to a
few Java applications. Architecture-independent (ARCH-INDP) studies are fast
and improve application diversity but assume unrealistic hardware.  
ARCH-INDP counts the writes to virtual memory without taking
cache effects into account~\cite{PLDI:2018:Shoaib}.  Our goal in this section
is to explore if the major conclusions hold regardless of the evaluation
methodology, including the reduction in PCM write rates.  Lacking PCM hardware,
we can not compare accuracy. 

We first compare emulation results to simulation results. We reproduce
simulation results from previous work~\cite{PLDI:2018:Shoaib}. Lack of
full-system support and long simulation times limit evaluation using the
simulator to 7 DaCapo benchmarks: {\sf lusearch}, {\sf lu.Fix}, {\sf avrora},
{\sf xalan}, {\sf pmd}, {\sf pmd.S}, and {\sf bloat}. We use two configurations
of simulated hardware: (1) 4 cores and 4\:MB LLC, and (2) 4 cores and 20\:MB
LLC, which more closely matches the emulation platform.  

Table~\ref{fig:sim} shows the percentage reduction in PCM write rates reported
by the three methodologies.  Intuition suggests \wrgc collectors should be more
effective with a smaller LLC. Smaller LLC absorbs fewer writes which increases the
writes to PCM memory.  The results from two simulated systems in
Table~\ref{fig:sim} confirm this intuition.
 
LLC pressure is high on the simulated system with a 4\:MB LLC and the emulation
platform running a multiprogrammed workload with four instances. The nursery
size of the seven simulated benchmarks is 4\:MB, and the nursery is highly
mutated, so using an emulation system with four applications creates more LLC
interference and thus more PCM writes, similar to the simulated system with a
4\:MB LLC.  The average reduction in PCM write rate for both cases is similar.  

Simulated results with a 4\:MB LLC are different from emulation results with
one and two program workloads. This is because the nursery is a major source of
writes which are absorbed by the 20\:MB LLC in the emulation platform. 

We observe that simulation results for \kgn and a 20\:MB LLC report a 4\%
reduction in PCM writes. On the contrary, emulation results report a 29\%
reduction in PCM writes. This discrepancy is due to full-system effects in the
emulation platform. When emulating \kgn, we place explicit memory
allocations by the C and C++ libraries in DRAM. In \pcm, these
allocations are placed in PCM, which is why emulation reports a larger
reduction in PCM writes for \kgn. 

\begin{table}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/compare-sim-archi-emulation.pdf}
	\caption{Comparing PCM write reduction using
simulation (SIM), architecture independent (ARCH-INDP) analysis, and emulation
(EMU) on 7 simulated benchmarks. N is the number of program instances in our multiprogrammed workloads. 
\textit{Simulation results confirm emulation results. The differences are due to
        cache sizes and full system effects.}
        }
	\label{fig:sim}
 	\centering
	\includegraphics[width=8cm]{./figures/compare-archi-emulation.pdf}
	\caption{Comparing PCM write reduction using
ARCH-INDP and EMU for
all benchmarks. \textit{ARCH-INDP over-reports the reduction in
PCM writes for 1 and 2 program workloads.  For a  balanced workload that
utilizes all cores on Socket 0, EMU results match ARCH-INDP results.}
        }
	\vspace*{-2.2ex}
	\label{fig:archi}
\end{table}

We observe another discrepancy between the simulation and the emulation results for
\kgb with a 20\:MB LLC. Simulation results suggest increasing the nursery size
to 12\:MB leads to more PCM writes compared to a 4\:MB nursery.  On the
other hand, emulation results suggest a 41\% reduction in PCM write rate.  This
discrepancy opens up opportunities for future investigations. Bugs in one or
both environments could misreport PCM writes with larger nurseries.
Alternatively, the discrepancy could arise because of full system effects --
the simulation environment isolates Java heap allocations from OS and native
library allocations. We leave investigating this discrepancy further to future
work.

ARCH-INDP results over predict reductions when compared to emulation results
with a single program instance. They over predict because ARCH-INDP counts
successive writes to PCM virtual memory as writes to PCM physical memory. In
reality, some of those writes are filtered by the CPU caches. When workloads
exhibit large LLC interference, such as multiprogrammed workloads with four
instances, emulation results are in the ballpark of ARCH-INDP. We observe the
same behavior when comparing ARCH-INDP and emulation results for all benchmarks
in Table~\ref{fig:archi}.

\noindent\textbf{Finding 1.} \textit{LLC size impacts PCM write rates.
Simulation and emulation results converge with similar nursery to LLC size
ratios.  Full-system effects may cause discrepancies for some configurations.} 

\noindent\textbf{Finding 2.} \textit{Architecture-independent metrics
over-report the reduction in PCM write rates.} 


\vspace*{-0.4em}
\subsection{Workload Analysis Using Emulation}

This section evaluates write-rationing garbage collectors much more fully than
prior work.  All trends previously observed for a narrow set of applications
and datasets are confirmed by emulation on a richer workload space. These
results make the case for using PCM as main memory even stronger. 

\paragraph{Garbage Collection Strategies for GraphChi}
%\label{sec:graphchi}

Contrary to prior work, emulation provides us the opportunity to evaluate \wrgc
collectors for GraphChi applications. We find these applications allocate large
objects frequently.  We show that combining the optimization for large objects
in \kgw with the heap organization of \kgn is effective.  This configuration
simplifies heap management and improves performance. GraphChi applications have
more mature space collections than DaCapo and Pjbb. We tease apart the impact
of the metadata optimization here by evaluating \kgw--MDO.  We also
evaluate \kgw--LOO to tease apart the impact of the large object optimization
from \kgw. 

\begin{figure}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/graphChiAnalysis.pdf}
	\caption{PCM write rates with various \wrgc collectors normalized to \pcm for GraphChi applications.  
		 \textit{\kgb + LOO works well for graph applications.}
        }
\vspace*{-1.7ex}
	\label{fig:graph-analysis}
\end{figure}

We show the write rates for single-program workloads normalized to \pcm in
Figure~\ref{fig:graph-analysis}. The absolute write rates increase for
multiprogrammed workloads but the normalized trends remain the same.  GraphChi
applications benefit from \kgn and \kgb that allocate new objects in a DRAM
nursery. This reduces write rates by 74\%, 75\%, and 91\% for {\sf PR}, {\sf
CC}, and {\sf ALS} respectively. \kgb uses a bigger nursery compared to \kgn
but still reduces write rates similar to \kgn. This confirms previous findings
with simulated benchmarks that we need novel heap organizations and other
optimizations to reduce PCM write rates further.

The graph applications allocate large objects and some follow the generational
hypothesis and thus benefit from the LOO optimization.  \kgn + LOO and \kgb +
LOO both reduce write rates on top of \kgn and \kgb respectively.  \kgn + LOO
reduces write rate by up to an additional 11\% compared to \kgn. \kgp + LOO is
even more effective for {\sf PR} and {\sf CC}: a 3\% additional reduction in
write rate. \kgn + LOO and \kgb + LOO are effective and have smaller execution
time overhead compared to \kgw.  Nevertheless, \kgw reduces write rates to PCM
more than \kgb + LOO for {\sf PR} and {\sf CC}.

Excluding LOO from \kgw (\kgw--LOO) increases the write rate because of large
object allocation in PCM of short-lived objects. Large object allocation in PCM further fills up the heap
quickly leading to more frequent mature collections. Mature collections are a source
of PCM writes because of the collector updates to the object mark
states.  The write rate increases by 3.3$\times$ for {\sf PR}, 2.6$\times$ for
{\sf CC}, and 1.5$\times$ for {\sf ALS}. 

Without the metadata optimization (MDO), PCM write rates increase, proving that
eliminating metadata writes during mature collections is effective. With one
instance of {\sf PR}, the write rate increases by 1.32$\times$ for {\sf PR} and
1.13$\times$for {\sf CC}. MDO benefits multiprogrammed workloads even more (not
shown).

\noindent\textbf{Finding 3.} \textit{Graph applications allocate many large
objects that benefit greatly from Kingsguard collectors that use the large
object optimization.}

\paragraph{Interference in Multiprogrammed Workloads}

\begin{figure}[t]
        \centering
	\includegraphics[width=8cm]{./figures/growth-pcm-only.pdf}

	\caption{Average PCM write rates with \pcm 
normalized to single-program write rates.
\textit{Writes rates increase with the number of benchmark instances. The write rates of Pjbb and
DaCapo grow super-linearly from 1 to 4 instances.}}
	\label{fig:growthpcmonly}
        \vspace{2mm}
	\centering
	\includegraphics[width=8cm]{./figures/growth-kgw.pdf}
	\caption{ Average PCM write rates with \kgw 
normalized to single-program write rates. \textit{The growth
in write rates is close to linear except Pjbb.} } 
	\label{fig:growthkgw}
\vspace*{-3.3ex}
\end{figure}

Long simulation times impede the evaluation of hybrid memories for
multiprogrammed Java workloads. The native execution speed of these workloads
on our emulation platform reveals interference patterns in the LLC which
results in writes to PCM memory.  Figure~\ref{fig:growthpcmonly} and
Figure~\ref{fig:growthkgw} shows the growth in average PCM write rates for \pcm
and \kgw for each benchmark suite and for all benchmarks.

We observe a variety of trends in write rates from the three suites.  On
average for \pcm, the increase in write rate from 1 to 2 program instances is
2.3$\times$, which is as we expect, but from 1 to 4 instances, the increase is
non-linear at 6.4$\times$.  DaCapo applications encounter high interference in
the LLC. The average increase in write rate from 1 to 4 instances for DaCapo is
9$\times$ (2.4$\times$ from 1 to 2 instances). The increase for Pjbb is even
higher. From 1 to 2 instances, the write rate increases by 5$\times$, and from
1 to 4 instances, the write rate increases by 12$\times$. GraphChi applications
on average show a linear trend. The increase in write rate from 1 to 4
instances is 1.9$\times$, and 3.5$\times$ from 1 to 4 instances.

Contrary to \pcm, \kgn and \kgw exhibit a linear increase in write rates from 1
to 2 and 4 program instances across the three suites: with 2 instances, the
increase is 1.8$\times$, 2.8$\times$, and 2.6$\times$ for DaCapo, Pjbb, and
GraphChi; with four instances, the increase is 3.1$\times$, 4.8$\times$, and
4.7$\times$ respectively. With \kgw, the increase is less than linear
except for Pjbb, which increases 6$\times$ with 4
program instances. 

\noindent\textbf{Finding 4.} \textit{PCM write rates grow super-linearly with
the number of concurrently running program instances for two popular Java
benchmark suites. Write rationing garbage collection significantly reduces the
growth in write rates.}


\paragraph{Modest versus Production Datasets}

The native speed of emulation admits larger datasets, previously unexamined.
The normalized writes rates for \pcm with large datasets shown in Figure~\ref{fig:dataset} 
follow three trends: The write rates of {\sf lusearch.fix} and {\sf xalan}
stay the same.  The write rates of {\sf avrora}, {\sf sunflow}, and {\sf PR} for
\pcm decrease by 0.7$\times$, 0.9$\times$, and 0.72$\times$ compared to default
datasets. The compute-to-write ratio of these applications increases with
larger inputs.  Conversely for {\sf pmd}, the write rate increases by
1.7$\times$.  Although absolute write rates change for some benchmarks, we
observe a similar reduction in PCM write rates with \kgn and \kgw. 

\noindent\textbf{Finding 5.} \textit{Production datasets sometimes shift the
balance between compute and memory-writes, changing PCM write rates.}
					
\begin{figure}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/dataset.pdf}
	\caption{Write rates with large datasets normalized to write rates with
default datasets (\pcm). \textit{ Normalized write rates may change with
production datasets. PCM write rate still reduces with \kgn and
\kgw. }}
	\label{fig:dataset}
\vspace*{-2.2ex}
\end{figure}


\paragraph{Classical versus Modern Suites}

\begin{figure}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/compare-suites.pdf}
	\caption{Average write rates in MB/s for DaCapo, Pjbb, and GraphChi
with \pcm. The numbers on top of Pjbb and GraphChi bars show the PCM write rate normalized to
DaCapo. \textit{Write rates increase for multiprogrammed
workloads. Pjbb and GraphChi have greater write rates compared to DaCapo.}}
	\label{fig:comp-suites}
\vspace*{-1.9ex}
\end{figure}

The DaCapo benchmark suite is the dominant choice for prior research in garbage
collection and some studies use Pjbb. We compare the average write rates of
Pjbb and GraphChi to DaCapo in Figure~\ref{fig:comp-suites}.
The average write rates of Pjbb and GraphChi are greater than DaCapo.  Pjbb
and GraphChi also have the largest heap sizes of all of our benchmarks.  The
average heap size of DaCapo is 100\:MB, Pjbb is 400\:MB, and GraphChi is
512\:MB. Pjbb has 1.7$\times$ the write rate of DaCapo with single-program
workloads.  Although we expect both Pjbb and GraphChi to have higher
write rates than DaCapo, it is interesting that GraphChi has a 4.7$\times$
higher write rate than DaCapo. 

\noindent\textbf{Finding 6.} \textit{Future studies on hybrid memories should
use a diversity of applications, including large heaps.}

The write rates of Pjbb and GraphChi are higher even for multiprogrammed
workloads. The difference is less pronounced compared to single-program
workloads because with four instances, DaCapo applications on average
experience greater interference in the LLC. The DaCapo rates increase
super-linearly whereas the increase is less than super-linear for Pjbb and
GraphChi. 

The difference in write rates between DaCapo and GraphChi is less pronounced
with \kgn and \kgw.  GraphChi has the same average write rate as DaCapo with
\kgn and single-program workloads. The write rate with two and four instances
is 1.5$\times$ and 1.6$\times$ higher than DaCapo. Thus, a large reason for
the gap in write rates of DaCapo and GraphChi for \pcm is the nursery writes.

The write rates of Pjbb with \kgn and \kgw are still higher compared to the
average DaCapo rates. For instance, with \kgw and single-program workloads, the
write rate of Pjbb is 3$\times$ that of DaCapo (2$\times$ for \kgn). For two
and four instances with \kgw, Pjbb incurs a 5.7$\times$ and 5$\times$ higher
write rate compared to DaCapo. 

\noindent\textbf{Finding 7.} \textit{Pjbb and GraphChi have higher write rates
than DaCapo.}

\paragraph{PCM Write Rates and Implications for Lifetime}
\label{sec:write-rates}

\begin{figure}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/abs-write-rates-1vm.pdf}
	\caption{Write rates in MB/s for one instance workloads. 
		 \textit{Benchmarks exhibit a range of write rates. 
                         Applications that allocate large objects frequently have the highest write rates.}
        }
	\label{fig:abs-wr-1vm}
	\vspace{2mm}
 	\centering
	\includegraphics[width=8cm]{./figures/abs-write-rates-4vm.pdf}
	\caption{Write rates in MB/s with 4 instance workloads.
        \textit{Write rates reduce significantly across all benchmarks with \wrgc collectors.}
        }
	\label{fig:abs-wr-4vm}
\vspace*{-3.3ex}
\end{figure}


We now show the raw write rates to PCM for our benchmarks and reduction in
write rates using \kgw.  We also discuss PCM lifetime in years for our
workloads. Lifetime is a linear function of write rate and PCM cell endurance.
We compute PCM lifetimes similar to prior work assuming a PCM write endurance of
10\:M writes per
cell~\cite{lee-pcm-isca,moin-pcm-isca,Moin-Start-Gap,PLDI:2018:Shoaib}.  We
assume a 32\:GB PCM system with hardware wear-leveling that delivers endurance
within 50\% of the theoretical maximum~\cite{Moin-Start-Gap}.
Table~\ref{tab:lifetime} shows worst-case PCM lifetimes in years for the three
benchmark suites. We choose the shortest lifetime of all benchmarks for DaCapo
and GraphChi. We only consider the fixed version of {\sf lusearch} in the
worst-case lifetime analysis.

Figure~\ref{fig:abs-wr-1vm} shows the write rates for \pcm and three \wrgc
configurations for single programs.  The average PCM write rate for \pcm is
126\:MB/s and write rates vary from 14\:MB/s for {\sf avrora} to 480\:MB/s for
{\sf PR}. {\sf lusearch} is excluded from the average.  Higher write rates of
GraphChi applications limit memory lifetime of \pcm to only 10.5 years with
single programs. The worst-case lifetime in DaCapo is 14 years for {\sf xalan}.
Wear-leveling and write filtering by LLC alone can make PCM last for 41 years
when running a single instance of Pjbb.

\begin{table}[b]
        \centering
        \vspace*{-1.3em}
        \includegraphics[width=8cm]{./figures/lifetime-table.pdf}
        \vspace{0.5mm}
        \caption{PCM lifetime in years for single-program (N=1) and 4-program (N=4) workloads. 
\textit{GraphChi and multiprogrammed workloads quickly wear out PCM. \wrgc collectors make PCM practical for all
workloads.}
        }
        \label{tab:lifetime}
\end{table}

\noindent\textbf{Finding 8.} \textit{Graph processing applications wear out PCM
much more quickly than DaCapo and Pjbb.} 

Of all the DaCapo benchmarks, {\sf lusearch} has the highest write rate of
320\:MB/s. Interestingly, {\sf lusearch.fix} fixes an allocation bug in the
original {\sf lusearch} and has a write rate of only 27\:MB/s. We also observe
a change in write rate between the two versions of {\sf pmd}. The original
benchmark has a write rate of 75\:MB/s on our platform. The version of the
benchmark that removes an input file for better scaling with the number of
threads, {\sf pmd.S}, has a write rate of 114\:MB/s. The execution time of {\sf
pmd.S} reduces significantly compared to {\sf pmd} leading to this higher write
rate.  

A widely used application in the DaCapo suite, {\sf eclipse}, has a write rate
of 50\:MB/s. This write rate is less than transaction ({\sf hsqldb} and 
Pjbb) and graph processing applications.  On the other hand, it is higher than
applications that do lexical analysis such as {\sf antlr} and {\sf bloat}.
{\sf ALS} with 170\:MB/s has the lowest write rate of the three GraphChi
applications.

\noindent\textbf{Finding 9.} \textit{Applications from different benchmark
suites and from different domains within a suite exhibit a variety of PCM write
rates. Applications that allocate large objects abundantly have higher write
rates.} 

\noindent\textbf{Finding 10.} \textit{Allocation behavior and input sets
influence write rates.}

\wrgc collectors significantly reduce PCM write rates across the three
benchmark suites. \kgn reduces the
average write rate by 50\% for single programs. The average write rate of \kgn
is 60\:MB/s. \kgb with its bigger nursery results in the same PCM write rates as \kgn.
GraphChi applications write much less to PCM with \kgn.  This
shows that the nursery is highly mutated even in modern graph processing
applications.  The benchmarks that do not benefit a lot from \kgn are those
that: (1) profusely allocate large short-lived data structures such as {\sf
lusearch} and {\sf xalan}, and (2) have more mature-object writes than nursery
writes, such as Pjbb~\cite{PLDI:2018:Shoaib}. 

\noindent\textbf{Finding 11.} \textit{Simply using DRAM for larger nurseries
does not reduce PCM write rates in hybrid memory systems.}

\kgw reduces the average write rate by 80\% and the raw average write rate is 24\:MB/s.
These low write rates greatly improve PCM lifetime.  PCM lifetime with \kgw for
single programs is practical across all three benchmark suites. For instance,
GraphChi applications with \kgw will wear out PCM after 72 years. PCM will also
be used for persistent storage which can have many more writes. The lifetimes
shown in Table~\ref{tab:lifetime} are therefore optimistic.

Figure~\ref{fig:abs-wr-4vm} shows PCM write rates of 4-program workloads.
Write rates increase a lot and up to 2.8\:GB/s for {\sf lusearch}.
\kgn reduces the write rate significantly for the 4-program {\sf lusearch}
workload.  The increase in write rates for multiprogrammed workloads has
implications for PCM lifetimes.  The average lifetime for the DaCapo suite
is not even 5 years. Lifetimes of Pjbb and GraphChi are worse.

\noindent\textbf{Finding 12.} \textit{Multiprogramming workloads can
wear out PCM memory in less than 5 years.}

\begin{figure}[bt]
 	\centering
	\includegraphics[width=8cm]{./figures/abs-write-rate.pdf}
	\caption{Average write rates in MB/s with varying number of benchmark
instances. The numbers on top of \kgn, \kgb, and \kgw bars show write rates
normalized to \pcm. \textit{Write rates increase with increasing number of
benchmark instances. \wrgc collectors are more effective for multiprogrammed
workloads.}}
	\label{fig:abs-wr}
\vspace*{-3.0ex}
\end{figure}

Figure~\ref{fig:abs-wr} compares average write rates of single-program and
multiprogrammed workloads with \pcm, \kgn, and \kgw.  Write rates reach close
to 1\:GB/s with two program instances and up to 2.8\:GB/s with four program
instances. Fortunately, write rates to PCM drop to less than 100\:MB/s on
average with \kgw across all workloads.  Figure~\ref{fig:abs-wr} also shows
that \kgn, \kgb, and \kgw are more effective in normalized terms for
multiprogrammed workloads.  Write rates increase due to interference in the LLC
and most of the interference is due to nursery writes.  Contrary to \kgn
reducing the write rate to PCM by 50\% with one instance, \kgn reduces the
write rate to PCM by 80\% with 4 program instances.  

\noindent\textbf{Finding 13.} \textit{Concurrently running applications in
multiprogrammed environments incur LLC interference due to nursery writes.
\wrgc collectors are highly effective in such environments.}

Table~\ref{tab:lifetime} shows that \wrgc collectors bring PCM lifetimes to
practical levels for multiprogrammed workloads. The worst-case lifetime is more
than 15 years for DaCapo, Pjbb, and GraphChi. Software and hardware approaches
together can make PCM a practical replacement for DRAM.

\paragraph{Execution Time}

Overall across single-program and multiprogrammed workloads and compared to
\kgn, \kgb slightly reduces the execution time, and \kgw increases the
execution time. The average reduction with \kgb is 3\%, and average increase
with \kgw is 10\% for single programs. The results are in the ballpark for
multiprogrammed workloads.  {\sf hsqldb} suffers the highest overhead of 28\%.
An exception with \kgw is {\sf bloat} whose execution time reduces up to 12\%.
The low survival rate of observer collections leads to fewer mature collections
which improve overall application performance.


\noindent\textbf{Finding 14.} \textit{There is a price to pay for severely
limiting writes to PCM.  KG-W's overhead ranges from 0-28\%.} 


