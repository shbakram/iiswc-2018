\vspace*{-1.3ex}
\section{Background}
\label{sec:background}

This section briefly discusses characteristics of NVM hardware and the role of
DRAM in hybrid DRAM-NVM systems. We then discuss write-rationing garbage
collection~\cite{PLDI:2018:Shoaib} that protect NVM from writes and prolongs
memory lifetime. We will evaluate write-rationing garbage collectors in
Section~\ref{sec:results} using our emulation platform.

\vspace*{-0.8ex}
\subsection{NVM Drawbacks and Hybrid Memory}

A promising NVM technology currently in production is phase change
memory (PCM)~\cite{xpoint}. PCM cells store information as the change in
resistance of a chalcogenide material~\cite{PCM-Vacum-Science}.  During a write
operation, electric current heats up PCM cells to high temperatures and the
cells cool down into an amorphous or a crystalline state that have different
resistances. The read operation simply detects the resistance of the cell.  PCM
cells wear out after 1 to 100 million writes because each write changes their physical
structure~\cite{lee-pcm-isca,moin-pcm-isca,PCM-Vacum-Science}.  Writes are also
an order of magnitude slower and consume more energy than in DRAM. Reading the PCM
array is up to 4$\times$ slower than DRAM~\cite{lee-pcm-isca}.  

Hybrid memories combine DRAM and PCM to mitigate PCM wear-out and tolerate its
higher latency. Frequently accessed data is kept in DRAM which results in better
performance and longer lifetimes compared to a \pcm system. The large PCM
capacity reduces disk accesses which compensates for its slow speed.

\vspace*{-0.7ex}
\subsection{Garbage Collection}

\paragraph{Generational Garbage Collection}
Managed languages such as Java, C\#, Python, and JavaScript use garbage
collection to accelerate development and reduce memory errors.
High-performance garbage collectors today exploit the generational hypothesis
that most objects die young~\cite{Ungar:1984:GSN}.  With generational
collectors, applications (mutators) allocate new objects contiguously into a
nursery. When allocation exhausts the nursery, a minor collection first
identifies live roots that point into the nursery, e.g., from global variables,
the stack, registers, and the mature space. It then identifies reachable
objects by tracing references from these roots. It copies reachable objects to
a mature space and reclaims all nursery memory for subsequent fresh allocation.
When the mature space is full, a full-heap (mature) collection collects the
entire heap. Recent work exploits garbage collectors to manage hybrid
memories~\cite{PLDI:2018:Shoaib,Gao:2013:UMR} and to improve PCM lifetimes.

\paragraph{Write-Rationing Garbage Collection}

Write-rationing collectors keep frequently written objects in DRAM in 
hybrid memories to improve PCM lifetime~\cite{PLDI:2018:Shoaib}. They come
in two main variants: The \emph{\basic} (\kgn) collector allocates nursery
objects in DRAM and promotes all nursery survivors to PCM. The nursery is highly
mutated and \kgn reduces write rates significantly compared to \pcm which leads
to a longer PCM lifetime.  \emph{\writers} (\kgw) monitors nursery survivors in
a DRAM observer space.  Observer space collections copy objects with zero
writes to a PCM mature space, and copy written objects to a DRAM mature space.
\kgw incurs a moderate performance overhead over \kgn due to monitoring and
extra copying of some nursery survivors but further improves PCM lifetime over
\kgn.

\kgw includes two additional optimizations to protect PCM from writes.
Traditional garbage collectors allocate large objects directly in a non-moving
mature space to avoid copying them from the nursery to the mature space.
Large Object Optimization (LOO) in \kgw allocates some large objects, chosen using a
heuristic, in the nursery giving them time to die. Like standard collectors, the
mutator allocates the remaining large objects directly in a PCM mature space.
The collector copies highly written large objects from PCM to DRAM during a
mature collection.  Garbage collectors also write to object metadata to mark
them live.  Marking live objects generates writes to PCM during a mature
collection. MetaData Optimization (MDO) places PCM object metadata in DRAM to
eliminate garbage collector writes to object metadata.

\wrgc collectors build on the best-performing collector in Jikes RVM:
generational Immix (GenImmix)~\cite{immix-blackburn}. GenImmix uses a copying
nursery and a mark-region mature space.  



