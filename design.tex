\vspace*{-0.6em}
\section{Design and Implementation}
\label{design}

This section describes the design and implementation of our emulator
for hybrid memory systems. It includes a hybrid-memory-aware memory manager
built on top of the standard NUMA hardware platforms widely available today.


%   Our goal is a flexible
% platform that enables evaluating a range of heap and collector configurations.
% We also provide details of our hardware platform in this section and discuss
% thread and memory mapping.

\vspace*{-1ex}
\begin{figure}[bt]
 	\centering
	\includegraphics[width=7.8cm]{./figures/heap-layout.pdf}
	\caption{The organization of our heap in hybrid memory.
        \textit{Memory composition is exposed to the language runtime. 
        Two free lists keep track of available virtual pages in DRAM and PCM.}
        }
	\vspace*{-1.5em}
	\label{fig:heap}
\end{figure}

\vspace*{-0.2em}
\subsection{Heap layout and management}

We allocate memory using the Linux OS calls for specifying a memory allocation
on a local or remote memory socket on a NUMA machine. We use the local socket
as the DRAM socket and the remote socket as the PCM socket.  We use a NUMA
specific version of the C memory allocator to call these routines. We modify
the Java Virtual Machine to call the C routines for DRAM and PCM allocation.
Figure~\ref{fig:heap} shows the high-level layout of our heap in hybrid memory.

We use Jikes RVM, but our approach generalizes to other JVMs. Jikes RVM is a
32-bit virtual machine, and each program has 4\:GB of virtual memory. The Linux
OS and system libraries use the low virtual memory for its own purposes. We use
the upper 2\:GB heap for the Java heap. This memory is sufficient for our
applications, although it is possible to use more than 2\:GB.  We partition the
heap into two parts. Each 1\:GB portion is logically divided into 4\:MB chunks
and managed independently by a free-list data structure. Figure~\ref{fig:heap}
shows {\sf Free-List-Hi} and {\sf Free-List-Lo} that keep track of free DRAM
and PCM memory respectively.  Each entry in the free-list contains
meta-information about the chunk: (1) size of the chunk, (2) status of the
chunk (free or in use), and (3) the current owner of the chunk. The lower 1\:GB
portion in virtual memory maps to PCM, and the upper portion maps to DRAM. 

Jikes RVM includes a memory management tool kit (MMTk) to manage the Java
heap.  Standard MMTk configurations flexibly manage portions of the heap using
different allocation and collection mechanisms. Each such portion is called a
space in MMTk terminology. For example, the nursery is a contiguous space and uses
a bump pointer allocator, and its memory is reclaimed using copying collection.
Each space in our implementation reserves virtual memory by requesting the
allocator associated with {\sf Free-List-Lo} or {\sf Free-List-High}. The
allocator finds a free chunk and returns the address to the requesting space.
The space then makes sure the chunk is mapped in physical memory. In our
approach, once a chunk is mapped in physical memory, we do not remove
its mapping in the OS page tables even if the chunk is no longer in use by the
requesting space. The chunk is recycled by the allocator when another space
requests a free chunk. We modify the chunk allocator to map memory on DRAM or PCM.

Alternative approaches are possible although their efficiency might be low.
For instance, a monolithic Java heap with a single free-list would require
unmapping free chunks from the physical memory. Because otherwise, a DRAM space
could end up using a logical chunk that is physically mapped in PCM.  The
flexibility of leaving the free chunks mapped in physical memory is a result of
our design with two free lists. 

Spaces such as the nursery have their address ranges reserved at boot-time.
On the other hand, mature spaces use a request mechanism to acquire chunks. These spaces share the
pool of available chunks with other spaces. Both types of spaces can be placed
in either DRAM or PCM. Each space specifies  DRAM or NVM as a flag in its
constructor.

Similar to the baseline design, we place the young generation (nursery) at one
side of the virtual memory.  This configuration enables the standard fast
boundary write-barrier for generational collection. Other contiguous spaces
such as the observer space in \kgw are placed next to the nursery. 

MMTk uses {\sf mmap()} for reserving virtual memory if none is available as
indicated by the free lists. To bind a virtual memory range to a particular
socket, we call {\sf mbind()} with the socket number after each call to {\sf
mmap()}.

\vspace*{-0.4em}
\subsection{Emulation on NUMA Hardware}


\paragraph*{\textbf{Hardware requirements}}

Our hardware requirement is a commodity NUMA platform with two sockets. We
require both sockets be populated with DRAM chips. Threads run on one socket,
referred to as the local DRAM socket. No threads execute on the other remote
PCM socket. Figure~\ref{fig:platform} shows an example NUMA hardware platform.
Allocation on Socket 0 (S0) is local to the threads and we use it to allocate
DRAM memory.  Memory accesses on Socket 1 (S1) are remote and emulates PCM.

\begin{figure}[b]
	\centering
	\vspace*{-1.5em}  
	\includegraphics[width=8cm]{./figures/platform.pdf}
	\caption{
		Our platform for hybrid memory emulation. \textit{The application
		and write rate monitor (WM) runs on socket 0. The memory on socket 0 is DRAM
		and socket 1 is PCM.}
          }
        \label{fig:platform}
\end{figure}



\paragraph*{\textbf{Space to Socket Mapping}}

\begin{table}[bt]
 	\centering
	\includegraphics[width=7.8cm]{./figures/heap-maps.pdf}
	\caption{
		 Spaces in \wrgc collectors and their mapping to
socket 0 (S0) or socket 1 (S1). S0 is DRAM and S1 is PCM. \kgn does
not use an observer space. \kgw uses a mature, large, and metadata
space in both DRAM and PCM.  \textit{Our virtual heap layout enables
a range of collector configurations for hybrid memory.}
        }
	\vspace*{-2.5em}  
	\label{fig:heap-maps}
\end{table}

Table~\ref{fig:heap-maps} shows the space to socket mapping in three of the
collectors we evaluate in this work on our emulation platform. 
\kgw and its variants use extra spaces in DRAM that are mapped to
socket 0 (S0). The observer space in \kgw is placed in DRAM and used to monitor
object writes. \kgw has a mature, large, and metadata space in both DRAM (S0)
and PCM (S1). \kgw--MDO does not include the metadata optimization (see
Section~\ref{sec:background}). Therefore, it does not use an extra metadata space
in DRAM.

The boot space contains the boot image runner that boots Jikes RVM and loads
its image files. Except for \pcm, we always place the boot image in DRAM
because we observe a large number of writes to it.


\paragraph*{\textbf{Thread to Socket mapping}}

When a particular thread uses the C or C++ library to allocate memory, the OS
places that memory on the socket where the thread is executing. Thus we have to
control to which socket each thread is mapped.  Our JVM calls down to these  C
and C++ libraries for allocation.  For the \wrgc configurations, we always bind
threads, including application and JVM service threads, to socket 0 (see
Figure~\ref{fig:platform}). \ignore{ Because the application executes on socket 0,
memory requests to socket 0 are local (DRAM), and memory accesses to socket 1
are remote (PCM).} When emulating a \pcm system, we bind threads to socket 1 for
accurately reporting write rates. We do not pin threads to specific cores and
use the default OS scheduler.

This work focuses on PCM lifetimes. PCM lifetime in years depends directly on
its write rate.  We measure write rates on our emulation platform using a write
rate monitor (WM in Figure~\ref{fig:platform}) that also runs on socket 0.
Threads are not pinned to specific cores and we use the default OS scheduler.
We experimentally find out that scheduling WM on socket 0 leads to more
deterministic write rate measurements. When scheduled on socket 1 and all
allocation isolated to socket 0, we continue to observe memory traffic on
socket 1.  
