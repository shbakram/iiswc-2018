\section{Experimental Methodology}
\label{sec:methodology}

\paragraph*{\textbf{Java Virtual Machine}}

We use Jikes RVM 3.1.2 because it uses software
practices that favor ease of modification, while still delivering good
performance~\cite{AAB+:05, alpern_et_al_2000, Oil:04,Frampton:2009}.  As a
comparison point, it took Hotspot~\cite{url:hs,hotspot} 10 years from the
publication of the G1 collector~\cite{Detlefs:2004} to its release. Jikes RVM
is a Java-in-Java VM with both a baseline and a just-in-time optimizing
compiler, but lacks an interpreter. Jikes RVM has a wide variety of garbage
collectors~\cite{mmtk,immix-blackburn,SBYM:13}. Its memory management tool kit
(MMTk)~\cite{mmtk} makes it easy to compose new collectors by combining
existing modules and changing the calls to the C and OS allocators. Jikes RVM also offers easy-to-modify write
barriers~\cite{YBFH:12} which makes it easy to implement a range of 
heap organizations.

\paragraph*{\textbf{Evaluation Metrics}}

We use two metrics to evaluate write-rationing garbage collectors: write
rate and execution time. PCM lifetime is directly proportional to its
write rate~\cite{moin-pcm-isca,Qureshi:2011:PLH,Qureshi:2009:ELS}. We report
execution time both for single application and multiprogrammed workloads.  Our
multiprogrammed workloads consist of multiple instances of the same
application.  On a properly provisioned platform, all instances should finish
execution at the same time. However, due to shared resources, there is
variation in the execution time of individual instances. We find
the variation on our platform to be low.

\paragraph*{\textbf{Measurement Methodology}}

We use best practices from prior work for evaluating Java applications on our
emulation platform~\cite{ha_et_al_2008,huang_et_al_2004}. To eliminate
non-determinism due to the optimizing compiler, we use replay compilation as used
in prior work. Replay compilation requires two iterations of a Java
application in a single experiment. During the first iteration, the VM
compiles each method to a pre-determined optimization level recorded in a prior
profiling run. The second measured iteration does not recompile methods leading
to steady-state behavior. We perform each experiment four times and report the
arithmetic mean. 

%\kathryn{? below: pcm-memory utility? How can this exist?} pcm here stands for processor counter monitor

We use Intel's Performance Counter Monitor framework for measuring write rates.
We use the {\sf pcm-memory} utility in the framework for measuring write rates.
We make modest modifications to support multiprogrammed
workloads and to make it compatible for use with replay compilation. In a
multiprogrammed workload, all applications synchronize at a barrier and start
the second iteration at the same time.  

\paragraph*{\textbf{Java Applications}}

We use 15 Java applications from three diverse sources: 11
DaCapo~\cite{blackburn_et_al_2006}, pseudojbb2005 (Pjbb)~\cite{pjbb}, and 3
applications from the GraphChi framework for processing graphs~\cite{graphchi}.
The GraphChi applications we use are: (1) {\sf page rank (PR)}, (2) {\sf
connected components (CC)}, and (3) {\sf ALS matrix factorization (ALS)}.
Compared to recent work~\cite{PLDI:2018:Shoaib}, we drop {\sf jython} as it
does not execute stably with our Jikes RVM configuration.  To improve benchmark
diversity, we use updated versions of {\sf lusearch} and {\sf pmd} in addition
to their original versions. {\sf lu.Fix} eliminates useless
allocation~\cite{YBF+:11}, and {\sf pmd.S} eliminates a scalability bottleneck
in the original version due to a large input file~\cite{DuBoisOopsla13}. Similar
to recent prior work, we run the multithreaded DaCapo applications, Pjbb, and
GraphChi applications with four application threads. 

Unless otherwise stated, we use the default datasets for DaCapo and Pjbb. Our
default dataset for GraphChi is as follows: for {\sf PR} and {\sf CC}, we
process 1\:M edges using the LiveJournal online social network~\cite{url:snap},
and for {\sf ALS}, we process 1\:M ratings from the training set of the Netflix
Challenge. The DaCapo suite comes packaged with large datasets for a subset of
the benchmarks. Our large dataset for GraphChi consists of 10\:M edges and
10\:M ratings.

Even though we do not include C and C++ benchmarks in this work, many of our
Java benchmarks exhibit the common behavior of mixing some C/C++ with Java
because Java standard features, such as IO, use C implementations.  For
example,  the DaCapo benchmarks execute a lot of C code~\cite{blink}.

\paragraph*{\textbf{Workload Formation}}

Multiprogrammed workloads reflect real-world server workloads because: (1) A
single application does not always scale with more cores, and (2)
multiprogramming helps amortize server real-estate and cost. Our
multiprogrammed workloads consist of two and four instances of the same
application. We do not restart applications after they finish execution.  To
avoid non-determinism due to sharing in the OS caches in multiprogrammed
workloads, we use independent copies of the same dataset for the different
instances.

\paragraph*{\textbf{Garbage Collectors and Configurations}}

We explore seven write-rationing garbage collectors.\ignore{ shown in
Table~\ref{tab:cfgs}.} 

Our collector configurations include \kgn, and a variant called
\kgb, that uses a bigger nursery than \kgn. \kgb and its variants use a 12\:MB nursery for DaCapo
and Pjbb, and a 96\:MB nursery for the GraphChi applications. The reason to
use \kgb is to understand if simply using large nurseries, equal to the sum of nursery
and observer space in \kgw, could reduce PCM write rates similar to \kgw.

For the GraphChi applications, we evaluate \kgn and \kgb with the Large Object
Optimization (LOO) to form \kgn + LOO and \kgb + LOO.  We include the original
\kgw and two variants:  one that removes LOO to form \kgw--LOO and one that
removes the MetaData Optimization (MDO) to form \kgw--MDO.  We configure the
\wrgc collectors to have the observer space twice as large as the nursery.
Prior work reports this to be a good compromise between tenured garbage and
pause time. 

We compare to \pcm with the baseline generational Immix
collector~\cite{immix-blackburn}. We configure the baseline collector similar
to prior work~\cite{PLDI:2018:Shoaib}. All our experiments use two garbage
collector threads. 

\paragraph*{\textbf{Nursery and Heap Sizes}}
Nursery size has an impact on performance, response time, and space
efficiency~\cite{Appel,mmtk,Ungar:1992,Zhao:2009:AWL}. Similar to prior
work~\cite{PLDI:2018:Shoaib}, we use a nursery of 4\:MB for DaCapo and Pjbb.
Although recent prior work uses a 4\:MB nursery for GraphChi applications, we
find a 32\:MB nursery improves performance, and we use this size for our
experiments with GraphChi applications. We use a modest heap size that is twice
the minimum heap size. Our heap sizes reflects those used in recent
work~\cite{Akram:2017:DEP,immix-blackburn,Sartor:2014:Scrubbing,Zhao:2009:AWL,yak-fang}.

\paragraph*{\textbf{Hardware Platform}}
Figure~\ref{fig:platform} shows the NUMA platform we use to emulate hybrid
memory.  Each socket contains one Intel E5-2650L processor with 8
physical cores each with two hyperthreads, for 16
logical cores. The platform has 132 GB of main memory.  Physical memory is
evenly distributed between the two sockets. We use all the DRAM channels in
both sockets. The 20\:MB LLC on each processor is shared by all cores. The
maximum bandwidth to memory is 51.2\:GB/s; more than the maximum bandwidth
consumed by any of our workloads. The two sockets are connected by QPI links
that support up to 8\:GT/s.  We use Ubuntu 12.04.2 with 3.16.0
kernel.


