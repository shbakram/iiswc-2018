\vspace*{-0.5ex}
\section{Introduction}
\label{intro}

Systems researchers and architects have long pursued bridging the speed gap
between processor, memory, and storage. Despite many efforts, the increase in
processor performance has consistently outpaced memory and storage speeds.
Recent advances in memory technologies have the potential to disrupt this speed
gap. 

On the storage side, emerging non-volatile memory (NVM) technologies  with
speed closer to DRAM and persistence similar to disk promise to narrow the
speed gap between processors and storage. \ignore{Recent work integrates NVM in
the storage hierarchy by engineering new filesystem abstractions, storage
stacks, programming models, wear-out mitigation schemes, and prototyping
platforms~\cite{Dulloor:2014:SSP,Condit:2009:BIT,Coburn:2011:NMP,Zhang:2015:MRH,Xu:2016:NLF,
Xu:2017:NFN,Volos:2011:MLP}.} Recent work engineers new filesystem
abstractions, storage stacks, programming models, wear-out mitigation schemes,
and prototyping platforms to integrate NVM in the
storage hierarchy~\cite{Dulloor:2014:SSP,Condit:2009:BIT,Coburn:2011:NMP,Zhang:2015:MRH,Xu:2016:NLF,Xu:2017:NFN,Volos:2011:MLP}.

On the main memory side, NVM promises abundant memory.  DRAM is facing scaling
limitations~\cite{Lim:2009,Mutlu:204:DRAM:Scaling}, and recent work combines
DRAM and NVM to form hybrid main memories~\cite{lee-pcm-isca,moin-pcm-isca}.
DRAM is fast and durable whereas NVM is dense and has low energy. Hardware
mitigates NVM wear-out in both its storage and memory roles using wear-leveling
and other approaches~\cite{moin-pcm-isca,lee-pcm-isca,Moin-Start-Gap,
Moin-Secure-PCM,Seznec-Secure-PCM}, while the OS keeps frequently accessed data
in
DRAM~\cite{ricardo-os,sally-semantics-hybrid,Zhang:2009,Oskin:2015:SAD,Mutlu:2017:UBH,Liu:2016:Memos}.
Recent work also explores managed runtimes to mitigate
wear-out~\cite{PLDI:2018:Shoaib,PASS:2018:Shoaib}, tolerate
faults~\cite{Gao:2013:UMR}, and keep frequently read objects in
DRAM~\cite{Wang:2016}. Collectively, prior research illustrates 
the substantial opportunities to exploit NVM across all layers including
software and language runtimes.

In this paper, we expand on the methodologies for evaluating NVM and hybrid
memories.  The dominant evaluation methodology in prior work is simulation; see
for example~\cite{moin-pcm-isca,lee-pcm-isca,Moin-Start-Gap,
Moin-Secure-PCM,Seznec-Secure-PCM,ricardo-os,sally-semantics-hybrid,Zhang:2009,Mutlu:2017:UBH}.
A few researchers have complemented simulation with architecture-independent
measurements~\cite{Sam:TC,PLDI:2018:Shoaib,PASS:2018:Shoaib}, but these
measurements have limited value because they miss important effects such as
CPU caching. This paper shows emulation confirms the results of simulation 
and architecture-independent analysis and enables researchers to explore
richer software configurations.

\begin{table}[b]
	\centering
	\vspace*{-1.2em}
	\includegraphics[width=8cm]{./figures/table-comparison.pdf}
	\vspace{0.5mm}
	\caption{ Comparing the strengths and weaknesses of evaluation
methodologies for hybrid memories. \textit{Emulation enables native exploration
of diverse workloads and datasets on realistic hardware.}
        }
        \label{tab:comp-methods}
\end{table}


The advantage of simulation is that it eases modeling new hardware features,
revealing how sensitive results are to architecture. Its major limitation is
that it is many orders of magnitude slower than running programs on real
hardware. Because time and resources are finite, it thus reduces the scope and
variety of architecture optimizations, application domains, implementation
languages, and datasets one can explore. Popular simulators also
trade off accuracy to speed up
simulation~\cite{carlson2014aeohmcm,Sanchez:2013:ZFA}.  Furthermore, frequent
hardware changes, microarchitecture complexity, and hardware's proprietary
nature make it difficult to faithfully model real hardware.

Other research evaluations are increasingly embracing emulation. For instance,
emulating cutting-edge hardware on commodity machines to model: asymmetric
multicores using frequency scaling~\cite{yinyang,Haque:2017}, die-stacked and
hybrid memory using DRAM~\cite{Oskin:2015:SAD,Wang:2016,Dulloor:2014:SSP}, and
wearable memory using fault injection software~\cite{Gao:2013:UMR}.
Recent work using emulation for exploring hybrid memory is either limited to
native languages~\cite{Oskin:2015:SAD,Dulloor:2014:SSP}, or is limited to
simplistic heap organizations in the case of managed languages~\cite{Wang:2016}
(See also Section~\ref{sec:related}). Table~\ref{tab:comp-methods} compares the
methodologies for evaluating hybrid memories, showing all can lead to insight
and that emulation has distinct advantages in speed and software
configuration.

We present the design, implementation, and evaluation of an emulation platform
that uses widely available commodity NUMA hardware to model hybrid
DRAM-NVM systems. \ignore{We show how emulation results complement simulation
results.} We use the local socket to emulate DRAM and the remote socket to
emulate NVM. All threads execute on the DRAM socket.  Our heap splits virtual
memory into DRAM and NVM virtual memory, which we manage using two free lists,
one for each NUMA node by explicitly specifying where to allocate memory in the
C standard library. We expose this hybrid memory to the garbage collector,
which directs the OS where in memory (which NUMA node) to map heap regions.
Contrary to most prior work, our platform handles both manual memory management
routines from the standard C library and memory management using an automatic
memory manager (garbage collector). We redesign the memory manager in the
popular Jikes research virtual machine (RVM) to add support for hybrid
memories. Our software infrastructure is publicly available at
\url{<link-anonymized-for-blind-reviewing>}.

We evaluate this emulation platform on recently proposed write-rationing
garbage collectors for hybrid memories~\cite{PLDI:2018:Shoaib}. Write-rationing
collectors keep highly mutated objects in DRAM in hybrid DRAM-NVM systems to
target longer NVM lifetime. We use 15 applications from three benchmark suites:
DaCapo, Pjbb, and GraphChi; two input datasets; seven garbage collector
configurations; and workloads consisting of one, two, and four application
instances executing simultaneously.  We find emulation results are very similar
to simulation results and platform-independent measurements in most cases, but
we can generate a lot more of them in the same amount of time and explore much
richer software configurations and workloads.  The emulator reveals trends not
identified previously by simulation and platform-independent measurements. We
summarize our key findings below.

\begin{itemize}

\item Simulation, emulation, and architecture-independent analysis reveal
similar trends in write rate reductions and other characteristics of garbage
collectors designed for hybrid memories, increasing our confidence in all the
evaluation methodologies.

\item Managed workloads use a lot of C/C++ code. Garbage collection strategies
for hybrid memories should protect against both writes to the managed heap and
writes to memory allocated using explicit C and C++ allocators.

%\item Expanding workloads to include transaction processing and large graph
%processing applications increases the scope of claims papers on hybrid memories
%can make.

\item Executing multiple applications simultaneously super-linearly increases
NVM write rates due to LLC interference, a configuration that is not practical
to explore in simulation. A major portion of the additional writes to memory are due 
nursery writes. \wrgc collectors isolate these writes on
DRAM and thus are especially effective in multiprogrammed environments.

\item Modern graph processing workloads use larger heaps and their write rates
are also higher than widely used Java benchmarks. Future work should include such
benchmarks when evaluating hybrid memories.

\item Addressing large objects' behaviors are essential to memory
managers for hybrid memories.  Graph applications can see huge reductions in
write rates when using \wrgc collectors, because they have a
lot of large objects that benefit from targeted optimizations. 

\item Changing a benchmark's allocation behavior or input changes write
rates. Future work should eliminate useless allocations and use a variety of
inputs for evaluating hybrid memories.


%\item Simply using larger nurseries in managed heaps does not reduce writes to
%NVM.

\item LLC size impacts write rates. Future work should use suitable workloads
with emulation on modern servers with large LLCs, or report evaluation
for a range of LLC sizes using simulation.

\item Graph applications wear PCM out faster than traditional Java benchmarks.
Multiprogramming workloads can also wear PCM out in less than 5 years. Write
limiting with \wrgc collectors brings PCM lifetimes to practical levels. 

\end{itemize}
